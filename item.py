#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  5 12:08:52 2018

@author: chelt
"""

class item(object):
    ''' Defines a box of a given length'''
    
    
    def __init__(self,n,s):
        '''creates the boxof a given length'''
        self.name = n
        self.size = s*[1]
    
    def getName(self):
        print(self.name)
    
    def getSize(self):
        return self.size
    
    def __str__(self):
        return self.name
        
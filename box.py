#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  5 10:35:38 2018

@author: chelt
"""

class box(object):
    ''' Defines a box of a given length'''
    
    
    def __init__(self,n,s,c):
        '''creates the boxof a given length'''
        self.name = n
        self.size = s
        self.cost = c
        self.vals = []
    
    def getName(self):
        print(self.name)
    def getVals(self):
        return self.vals
    
    def getSize(self):
        return self.size
    def getCost(self):
        return self.cost
    
    def check(box):
        '''checks how full the box is and returns an int'''
        filled = 0
        for i in box.getSize():
            if i == 0:
                filled += 0
            elif i == 1:
                filled +=1
                    
        return filled


    def fillboxfull(self,i):
        
        self.vals = i*[1]
    
    